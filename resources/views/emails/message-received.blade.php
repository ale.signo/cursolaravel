<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mensaje recibido</title>
</head>
<body>
    <h1>Recibiste un mensaje de: {{ $msg['name'] }} - {{ $msg['email'] }} </h1>
    <h2>Asunto: {{ $msg['subject'] }}</h2>
    <p>Contenido del mensaje: {{ $msg['content'] }}</p>

</body>
</html>