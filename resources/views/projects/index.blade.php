@extends('layouts.layout')

@section('title', 'Portfolio')

@section('content')
    <section class="container">
        <h1>Portfolio</h1>
        @auth
            <a href="{{ route('projects.create') }}" class="btn btn-primary mb-3">Crear nuevo proyecto</a>
        @endauth
        <ul>
            @forelse($projects as $project)
                <li>
                    <a href="{{ route('projects.show', $project) }}">{{ $project->title }}</h2></a>
                    <p>{{ $project->description }}</p>
                    <p>{{ $project->created_at->diffForHumans() }}</p>
                </li>
            @empty
                <li>No hay proyectos para mostrar.</li>
            @endforelse
        </ul>

        {{ $projects->links() }}
    </section>
@endsection