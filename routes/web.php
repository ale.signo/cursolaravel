<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\ProjectController;


//HomeController
Route::get('/', [HomeController::class, 'index'])
    ->name('home');
Route::get('/quienes-somos', [HomeController::class, 'about'])
    ->name('about');
Route::get('/contacto', [HomeController::class, 'contact'])
    ->name('contact');

//ProjectController
Route::resource('proyectos', ProjectController::class)
    ->names('projects')
    ->parameters(['proyectos' => 'project']);

//MessagesController
Route::post('/contacto', [MessageController::class, 'store'])
    ->name('contact.store');

//Login -- Register
Auth::routes(['register' => false]);

