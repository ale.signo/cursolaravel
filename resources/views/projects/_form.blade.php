@csrf
<div class="form-group">
    <label for="title">Título</label>
    <input type="text" name="title" id="title" class="form-control" value="{{ old('title', $project->title) }}">
</div>
<div class="form-group">
    <label for="url">Url del proyecto</label>
    <input type="text" name="url" id="url" class="form-control" value="{{ old('url', $project->url) }}">
</div>
<div class="form-group mb-4">
    <label for="description">Descripción</label>
    <textarea class="form-control" name="description" id="description" rows="3">{{ old('description', $project->description) }}</textarea>
</div>
<button class="btn btn-primary" type="submit">{{ $btnText }}</button>