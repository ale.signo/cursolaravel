<?php

namespace App\Http\Controllers;

use App\Mail\MessageReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    public function store() {
        $message = request()->validate([
            'name'      => 'required',
            'email'     => 'required|email',
            'subject'   => 'required',
            'content'   => 'required|min:3'
        ],[
            'name.required' => 'Debes completar tu nombre'
        ]);

        Mail::to('ale@laravel.com')->queue(new MessageReceived($message));
        return redirect()->back()->with('status', 'Recibimos tu mensaje');
    }
}
