@extends('layouts.layout')

@section('title', 'Portfolio | Crear nuevo proyecto')

@section('content')
    <section class="container">
        <h1>Crear nuevo proyecto</h1>
        @include('partials.validation-errors')
        <form action="{{ route('projects.store') }}" method="POST">
            @include('projects._form', [
              'btnText' => 'Crear'
          ])
        </form>
    </section>
@endsection