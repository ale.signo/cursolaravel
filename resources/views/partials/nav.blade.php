<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Curso de laravel</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item active">
                <a class="nav-link {{ setActive('home') }}" href="{{ route('home') }}">@lang('Home')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ setActive('about')}}" href="{{ route('about') }}">@lang('About')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ setActive('projects.index')}}" href="{{ route('projects.index') }}">@lang('Projects')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ setActive('contact')}}" href="{{ route('contact') }}">@lang('Contact')</a>
            </li>
            @guest
                <li class="nav-item">
                    <a class="nav-link {{ setActive('login')}}" href="{{ route('login') }}">@lang('Login')</a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                </li>
            @endguest
      </div>
    </div>
  </nav>

{{-- Formulario para cerrar sesion --}}
<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</form>