@extends('layouts.layout')

@section('title', 'Projectos | ' . $project->title)

@section('content')
    <section class="container">
        <h1>{{ $project->title }}</h1>
        @auth
            <a href="{{ route('projects.edit', $project) }}" class="btn btn-primary">Editar proyecto</a>
            <form action="{{ route('projects.destroy', $project) }}" method="POST">
                @csrf @method('DELETE')
                <button class="btn btn-danger">Borrar</button>
            </form>
        @endauth
        <p>{{ $project->description }}</p>
        <p>{{ $project->created_at->diffForHumans() }}</p>
    </section>
@endsection