@extends('layouts.layout')

@section('title', 'Inicio')

@section('content')
<section class="container">
    <h1>Pagina home</h1>
    @auth
        {{ auth()->user()->name}}
    @endauth
</section>
@endsection