@extends('layouts.layout')

@section('title', 'Contacto')

@section('content')
<section class="container">
    <h1>@lang('Contact')</h1>
    <form action="{{ route('contact.store') }}" method="POST">
        @csrf
        <div class="form-group">
          <label for="name">Nombre</label>
          <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
          @error('name')
            <p class="alert alert-danger">{{ $message }}</p>
          @enderror
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
          @error('email')
            <p class="alert alert-danger">{{ $message }}</p>
          @enderror
        </div>
        <div class="form-group">
          <label for="subject">Asunto</label>
          <input type="text" name="subject" id="subject" class="form-control" value="{{ old('subject') }}">
          @error('subject')
            <p class="alert alert-danger">{{ $message }}</p>
          @enderror
        </div>
        <div class="form-group mb-4">
            <label for="content">Mensaje</label>
            <textarea class="form-control" name="content" id="content" rows="3">{{ old('content') }}</textarea>
            @error('content')
            <p class="alert alert-danger">{{ $message }}</p>
            @enderror
        </div>
        <button class="btn btn-primary" type="submit">Enviar</button>
    </form>
</section>
@endsection