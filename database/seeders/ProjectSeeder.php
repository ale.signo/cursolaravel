<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            [
                'id' => 1,
                'title' => 'Mi primer proyecto de prueba',
                'url' => 'mi-primer-proyecto-de-prueba',
                'description' => 'Descripcion del primer proyecto de prueba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'title' => 'Mi segundo proyecto de prueba',
                'url' => 'mi-segundo-proyecto-de-prueba',
                'description' => 'Descripcion del segundo proyecto de prueba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'title' => 'Mi tercer proyecto de prueba',
                'url' => 'mi-tercer-proyecto-de-prueba',
                'description' => 'Descripcion del tercer proyecto de prueba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
